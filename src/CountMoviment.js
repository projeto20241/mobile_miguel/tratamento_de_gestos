import React, { useRef, useEffect, useState } from "react";
import { View, Dimensions, PanResponder, Image, StyleSheet } from "react-native";

const images = [
  { id: 1, uri: "https://via.placeholder.com/200" },
  { id: 2, uri: "https://via.placeholder.com/200/FF5733" },
  { id: 3, uri: "https://via.placeholder.com/200/33FF57" },
  { id: 4, uri: "https://via.placeholder.com/200/5733FF" },
];

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

const CountMovement = () => {
  const [count, setCount] = useState(0);

  const gestureThreshold = screenHeight * 0.25;
  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dy < -gestureThreshold && count < images.length - 1) {
          setCount((prevCount) => prevCount + 1);
        }
      },
    })
  ).current;

  return (
    <View style={styles.container}>
      <View {...panResponder.panHandlers} style={styles.carousel}>
        {images.map((image, index) => (
          <Image
            key={index}
            source={{ uri: image.uri }}
            style={[
              styles.image,
              { transform: [{ translateY: count === index ? 0 : screenHeight }] },
            ]}
          />
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  carousel: {
    width: screenWidth,
    height: screenHeight,
    flexDirection: "row",
  },
  image: {
    width: screenWidth,
    height: screenHeight,
    position: "absolute",
    top: 0,
    left: 0,
  },
});

export default CountMovement;
